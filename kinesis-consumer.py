import boto3
import io
import json
import pandas as pd

key_id = '<key>'
secret_key = '<secret-key>'
region_name = 'ap-southeast-1'
# rolearn = 'arn:aws:sts::330084576195:role/ProdDataScience'
# role_session_name = "AssumeRoleSession1"


# #### ONLY USE FOR SWITCHING ROLES ############################################################################
# boto_sts = boto3.client('sts', aws_access_key_id=key_id, aws_secret_access_key=secret_key)
# stsresponse = boto_sts.assume_role(RoleArn=rolearn, RoleSessionName=role_session_name)
# credentials = stsresponse['Credentials']
#
# s_session = boto3.Session(aws_access_key_id=credentials['AccessKeyId'], \
#                         aws_secret_access_key=credentials['SecretAccessKey'], \
#                         aws_session_token=credentials['SessionToken']
#                         )

# session_cred = s_session.get_credentials()

# ks = s_session.client('kinesis', region_name=region_name)
################################################################################################################


ks = boto3.client('kinesis', region_name=region_name, aws_access_key_id=key_id, aws_secret_access_key=secret_key)

stream_name = 'stg-kinesis-stream'
partition_key = "1"

response = ks.describe_stream(StreamName=stream_name)

my_shard_id = response['StreamDescription']['Shards'][0]['ShardId']

shard_iterator = ks.get_shard_iterator(StreamName=stream_name, ShardId=my_shard_id, ShardIteratorType='LATEST')

my_shard_iterator = shard_iterator['ShardIterator']

record_response = ks.get_records(ShardIterator=my_shard_iterator, Limit=2)

while 'NextShardIterator' in record_response:
    record_response = ks.get_records(ShardIterator=record_response['NextShardIterator'], Limit=2)

    if record_response['Records']:
        print(record_response['Records'])
